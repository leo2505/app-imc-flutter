import 'package:flutter/material.dart';

class Calcular extends StatefulWidget {
  String nomePaciente;
  int idadePaciente;
  double pesoPaciente;
  double alturaPaciente;

  Calcular(this.nomePaciente, this.idadePaciente, this.pesoPaciente,
      this.alturaPaciente);

  @override
  _CalcularState createState() => _CalcularState();
}

class _CalcularState extends State<Calcular> {
  double imc;
  String nomePaciente;
  Color corApp = Colors.blue;
  String resultadoIMC;
  @override
  void initState() {
    super.initState();
    nomePaciente = widget.nomePaciente;
    imc =
        (widget.pesoPaciente / (widget.alturaPaciente * widget.alturaPaciente));
    if(imc < 17){
      resultadoIMC = "Você precisa comer mais... está muito magro";
    } else if(imc >= 17 && imc < 18.5){
      resultadoIMC = "Continue comendo que você chega lá!";
    } else if (imc >= 18.5 && imc < 25){
      resultadoIMC = "Tá no peso!";
    } else if (imc == 25){
      resultadoIMC = "Melhor começar a cuidar!!";
    } else if (imc > 25 && imc <= 30){
      resultadoIMC = "Está um pouco acima do peso";
    } else if (imc >= 30 && imc <= 35){
      resultadoIMC = "Não vou mentir... você está com obesidade nível I.";
    } else if (imc >= 35 && imc <= 40){
      resultadoIMC = "Está com obesidade nível II";
    } else if (imc > 40){
      resultadoIMC = "Se a vida fosse um jogo, você era top level: Obesidade nível III, conhecida como obseidade mórbida";
    }
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: corApp,
        title: Text("Cálculo IMC"),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.all(10),
        child: Center(
          child: Column(
            children: <Widget>[
              Center(
                child: Text(
                  this.nomePaciente + ", seu imc é: ",
                  style: TextStyle(
                    fontSize: 20,
                  ),
                ),
              ),
              Center(
               child: Text(
                  this.imc.toStringAsPrecision(4),
                  style: TextStyle(
                    fontSize: 30,
                    fontWeight: FontWeight.w800,
                  ),
                ),
              ),
              Center(
                child: Text(
                  this.resultadoIMC,
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.w800,
                  ),
                ),
              ),
              Center(
                child: Text(
                  "Feito com"
                ),
              ),
              Center(
                child:
                Image.asset("images/logo.png"),
              )
            ],
          ),
        ),
      ),
    );
  }
}
