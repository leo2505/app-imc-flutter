import 'package:flutter/material.dart';
import 'package:imc_app/calcular.dart';

void main() {
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    home: CalculaIMC(),
  ));
}

class CalculaIMC extends StatefulWidget {
  @override
  _CalculaIMCState createState() => _CalculaIMCState();
}

class _CalculaIMCState extends State<CalculaIMC> {
  GlobalKey<FormState> formkey = GlobalKey<FormState>();

  TextEditingController nomePessoa = TextEditingController();
  TextEditingController idadePessoa = TextEditingController();
  TextEditingController pesoPessoa = TextEditingController();
  TextEditingController alturaPessoa = TextEditingController();

  var focusNome = new FocusNode();
  var focusIdade = new FocusNode();
  var focusPeso = new FocusNode();
  var focusAltura = new FocusNode();

  Color corApp = Colors.blue;
  double pesoPaciente;
  int idadePaciente;
  double alturaPaciente;

  void initState() {
    super.initState();
    //nomePessoa.text = "";
    //idadePessoa.text = "";
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: corApp,
        title: Text("Cálculo IMC"),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.all(10),
        child: Form(
          key: formkey,

          child: Center(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                TextFormField(
                  focusNode: focusNome,
                  autofocus: true,
                  textInputAction: TextInputAction.next,
                  validator: (valor) {
                    if (valor.isEmpty) {
                      FocusScope.of(context).requestFocus(focusNome);
                      return "Informe o nome";
                    } else {
                      return null;
                    }
                  },
                  controller: nomePessoa,
                  keyboardType: TextInputType.text,
                  decoration: InputDecoration(
                    contentPadding: EdgeInsets.only(left: 10),
                    suffixIcon: Icon(Icons.assignment_ind),
                    labelText: "Nome",
                  ),
                ),
                TextFormField(
                  focusNode: focusIdade,
                  autofocus: true,
                  textInputAction: TextInputAction.next,
                  validator: (valor) {
                    if (valor.isEmpty) {
                      FocusScope.of(context).requestFocus(focusIdade);
                      return "Informe o nome";
                    } else {
                      return null;
                    }
                  },
                  controller: idadePessoa,
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                    contentPadding: EdgeInsets.only(left: 10),
                    suffixIcon: Icon(Icons.calendar_today),
                    labelText: "Idade",
                  ),
                ),
                TextFormField(
                  focusNode: focusPeso,
                  autofocus: true,
                  textInputAction: TextInputAction.next,
                  validator: (valor) {
                    if (valor.isEmpty) {
                      FocusScope.of(context).requestFocus(focusPeso);
                      return "Informe a altura";
                    } else {
                      return null;
                    }
                  },
                  controller: pesoPessoa,
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                    contentPadding: EdgeInsets.only(left: 10),
                    suffixIcon: Icon(Icons.assignment_turned_in),
                    labelText: "Peso",
                  ),
                ),
                TextFormField(
                  focusNode: focusAltura,
                  autofocus: true,
                  textInputAction: TextInputAction.next,
                  validator: (valor) {
                    if (valor.isEmpty) {
                      FocusScope.of(context).requestFocus(focusAltura);
                      return "Informe a Altura";
                    } else {
                      return null;
                    }
                  },
                  controller: alturaPessoa,
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                    contentPadding: EdgeInsets.only(left: 10),
                    suffixIcon: Icon(Icons.format_size),
                    labelText: "Altura",
                  ),
                ),
                RaisedButton(
                    textColor: Colors.white,
                    color: Colors.blue,
                    child: Text("Calcular IMC"),
                    onPressed: () {
                      pesoPaciente = double.parse(pesoPessoa.text);
                      idadePaciente = int.parse(idadePessoa.text);
                      alturaPaciente = double.parse(alturaPessoa.text);
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => Calcular(nomePessoa.text, idadePaciente, pesoPaciente, alturaPaciente)));
                    }),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
